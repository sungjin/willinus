'use strict'

const {app, BrowserWindow, ipcMain} = require('electron')

let win, printWindow

app.on('ready', function() {

  win = new BrowserWindow({
    fullscreen: true,
    frame: true
  })

  win.setMenu(null)

  win.loadURL('file://' + __dirname + '/app/index.html')

  printWindow = new BrowserWindow({ show: false })
  printWindow.loadURL('file://' + __dirname + '/app/printWindow.html')

})

ipcMain.on('content', (event, arg) => {
  printWindow.send('content', arg)
})

ipcMain.on('print', (event) => {
  printWindow.webContents.print({
    silent: false
  })
})
