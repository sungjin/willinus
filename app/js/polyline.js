'use strict'

const config = require('../../config.json')

module.exports = class Polyline {

  constructor(_x, _y, _w, _h, d) {
    let p = this.p = config.polyline.padding
    this.m = config.polyline.max
    this.x = _x + p
    this.y = _y + p
    this.w = _w - p - p
    this.h = _h - p - p
    this.lines = d
  }

  render() {
    let tags = []
    for(let i in this.lines) {
      let line = this.lines[i]
      let points = this._draw(line)
      let tag = this._taglize(points, config.polyline.colors[i])
      tags.push(tag)
    }

    return tags.join('\n')
  }

  _draw(sd) { // single data
    let points = []
    let l = sd.length
    let step = this.w / (l - 1)
    let localx = 0

    for(let i in sd) {
      let d = sd[i]
      let localy = (1 - (d / this.m)) * this.h
      points.push(this._rp2s(localx, localy))
      localx += step
    }

    return points
  }

  _rp2s(x, y) { // relative point to absolute pathstring
    return (x + this.x) + ',' + (y + this.y)
  }

  _taglize(points, color) {
    let p = points.join(' ')
    return `<polyline fill="none" stroke="${color}" points="${p}" />`
  }

}
