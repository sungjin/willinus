'use strict'

const {ipcRenderer} = require('electron')
const ejs = require('ejs')
const fs = require('fs')

module.exports = class Printer {

  constructor(config, debug) {

    this.config = config

    let f = fs.readFileSync('app/print.html')
    this.template = ejs.compile(f.toString())

  }

  load(result) {
    ipcRenderer.send('content', this.template(result))
    return this
  }

  print() {
    ipcRenderer.send('print')
    return this
  }

}
