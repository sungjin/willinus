'use strict'

const EventEmitter = require('events')

module.exports = class Movie extends EventEmitter {

  constructor(config) {
    super()

    this.config = config

    this.completed = false
  }

  setOrder(order) {
    this.queue = order.map((o, i) => {
      let r = this.config.types[o]
      r.n = i
      r.id = o
      return r
    })
  }

  next() {

    if(this.queue.length == 0) {
      this.done()
      return
    }

    let job = this.queue.shift()

    page.slideVideo(job, (status) => {
      if(status == 'title_end') {
        this.next()
      } else if(status == 'movie_end') {
        this.emit('end', { id: job.id })
      }
    })

    this.emit('start', {
      id: job.id
    })
  }

  done() {
    if(this.completed) return
    this.completed = true
    this.emit('done')
  }

}
