'use strict'

const EventEmitter = require('events')

module.exports = class Tournament extends EventEmitter {

  constructor(option) {

    super()

    this.types = option.types

    this.reset()
  }

  reset() {
    this.order = this.generate(this.types.length)
    this.nextOrder = []

    this.step = 0
    this.completed = false

    this.score = {}
    this.order.forEach((o) => { // init score with 0
      this.score[o] = 0
    })
    this.result = {
      8: [],
      2: []
    }
  }

  generate(length) {
    return Array.apply(null, {length: length})
                .map(Number.call, Number)
                .sort(() => 0.5 - Math.random()) // <- semi-random sort

  }

  work() {
    this.step++

    if(this.order.length == 0) {
      this.order = this.nextOrder
      this.nextOrder = []
    }
    if(this.order.length == 1) {
      this.done()
      return
    }
    let n1 = this.order.shift()
    let n2 = this.order.shift()
    let q1 = this.types[n1]
        q1.n = n1
    let q2 = this.types[n2]
        q2.n = n2

    this.emit('step', {
      current: this.step,
      total: this.types.length - 1
    })

    page.slideQuestion(q1, q2, (score) => {
      if(this.completed) {
        return
      }
      this.score[score.win] += 2
      this.score[score.lose] += 1
      this.nextOrder.push(score.win)
      this.hardcoded(score)

      this.work()
    })
  }

  hardcoded(score) {
    if(this.step <= 4) {
      this.result[8].push(score.lose)
      this.result[8].push(score.win)
    } else if(this.step == 5 || this.step == 6) {
      this.result[2].push(score.win)
    } else if(this.step == 7) {
      this.result[1] = score.win
    }
  }

  done() {
    page.slideTResult(this.result, () => {
      this.emit('done', {
        score: this.score,
        order: Object.keys(this.score).sort((a, b) => this.score[b] - this.score[a]),
      })
    })
    this.completed = true
  }

}
