'use strict'

const ejs = require('ejs')
const fs = require('fs')

const {remote} = require('electron')

const polyline = require('./polyline')

module.exports = class Pager {

  constructor(config) {
    let parser = new DOMParser()

    let f = fs.readFileSync('app/demo.svg')
    this.template = ejs.compile(f.toString())

    this.container = document.getElementById('slider-container')
    this.config = config

  }

  slideStartPage(callback) {
    let t = this._render({
      type: 'intro',
      data: this.config.pages[0]
    })

    this._push(t)

    this.updateFooter({
      label: '시작',
      noCapitalize: true,
      callback: callback
    })
  }

  slideQuestion(q1, q2, callback) {
    let n1 = q1.n, n2 = q2.n
    let t = this._render({
      type: 'vs',
      background: [q1.color, q2.color],
      data: [q1, q2]
    })

    this.konami(t)

    let clicked = false

    t.getElementById('clickpad1')
     .addEventListener('click', function() {
       if(clicked)  return
       else clicked = true
       this.classList.add('active')

       setTimeout(() => {
         callback({
           win: n1,
           lose: n2
         })
       }, 500)
     })
    t.getElementById('clickpad2')
     .addEventListener('click', function() {
       if(clicked)  return
       else clicked = true
       this.classList.add('active')

       setTimeout(() => {
         callback({
           win: n2,
           lose: n1
         })
       }, 500)
     })

     this.updateHeader({
       title: 'WannaB',
       subtitle: '토너먼트'
     })

    document.querySelector('footer').classList.remove('disabled')

    this._push(t)
  }

  slideTResult(result, callback) {
    let t = this._render({
      type: 'tresult',
      tresult: result
    })

    this._push(t)

    this.updateFooter({
      label: '다음',
      noCapitalize: true,
      callback: callback
    })
  }

  slideTitle(q, timeout, callback) {
    if(callback === undefined) {
      callback = timeout
      timeout = 7000
    }
    let t = this._render({
      type: 'title',
      background: q.color,
      audio: true,
      data: q
    })

    this.konami(t)
    this._push(t)

    this.updateHeader({
      title: 'WannaB'
    })

    setTimeout(() => {
      callback()
    }, timeout)
  }

  slideVideo(q, callback) {
    let ended = false
    let end = () => {
      if(ended)  return
      else ended = true

      this.updateHeader('disable')
      this.updateFooter('disable')

      callback('movie_end')
      this.slideTitle(q, function(){
        callback('title_end')
      })
    }

    let t = this._render({
      type: 'video',
      background: false,
      data: q
    })

    t.getElementsByTagName('video')[0].addEventListener('ended', end)

    this.updateHeader({
      title: 'WannaB'
    })
    this.updateFooter({
      label: '넘기기',
      noCapitalize: true,
      callback: end
    })

    this.konami(t)
    this._push(t)

  }

  slideResult(result, callback) {
    let t = this._render({
      type: 'result',
      Polyline: polyline,
      root: this.config.videoRoot,
      types: this.config.types,
      background: undefined,
      result: result
    })

    this.konami(t)
    this._push(t)

    this.updateHeader('disable')
    document.querySelector('.print').classList.remove('disabled')

    this.updateFooter({
      label: '완료',
      noCapitalize: true,
      callback: callback
    })
  }

  updateHeader(options) {
    let h = document.querySelector('header')
    let p = h.getElementsByTagName('p')[0]

    if(options == 'disable') {
      h.classList.add('disabled')
      return
    }

    h.getElementsByTagName('h1')[0].textContent = options.title

    if(options.subtitle) {
      p.classList.remove('hidden')
      p.textContent = options.subtitle
    } else {
      p.classList.add('hidden')
    }
  }

  updateFooter(options) {
    let f = document.querySelector('footer')
    let flag = false
    if(options == 'disable') {
      f.classList.add('disabled')
      return
    } else if(typeof options === 'string') {
      f.classList.remove('no-cap')
      return
    }

    if(options.noCapitalize) {
      f.classList.add('no-cap')
    } else {
      f.classList.remove('no-cap')
    }

    f.getElementsByTagName('div')[0].textContent = options.label

    if(options.callback) {
      f.addEventListener('click', () => {
        if(flag) return false
        else {
          flag = true
          options.callback()
        }
      })
    }

    f.classList.remove('disabled')
  }

  _render(o) {
    return this._parse(this.template(o)).firstElementChild
  }

  _parse(s) {
    var parser = new DOMParser()
    return parser.parseFromString(s, 'image/svg+xml')
  }

  _push(t) {
    let current = document.querySelector('.page.current')

    t.classList.add('preparing')
    this.container.appendChild(t)

    if(current) {
      current.classList.remove('current')
      current.classList.add('removing')
      setTimeout(() => {
        current.remove()
      }, 300)
    }
    // query once, to clarify `left` value
    window.getComputedStyle(t).left

    t.classList.remove('preparing')
    t.classList.add('current')
  }

  // konami command
  konami(svg) {
    if(!this.config.konami) {
      return
    }

    let command = this.config.konami.command || [
      'ue', 'ue', 'sita', 'sita',
      'hidari', 'migi', 'hidari', 'migi',
      'b', 'a']
    let pressed = []
    let pads = svg.querySelectorAll('.clickpad.konami')

    let what = {
      'exit': () => process.exit(),
      'devtools': () => remote.getCurrentWebContents().openDevTools()
    }[this.config.konami.action]

    Array.prototype.forEach.call(pads, (pad) => {
      pad.addEventListener('click', function() {
        this.classList.add('active')
        setTimeout(() => {
          this.classList.remove('active')
        }, 200)

        let input = this.getAttribute('data-command')
        pressed.push(input)
        for(let i in pressed) {
          if(pressed[i] != command[i]) {
            // fail
            pressed = []
          } else if (pressed[i] == command[i] && i == (command.length - 1)) {
            // fire
            what && what()
            pressed = []
          }
        }
      })
    })
  }

}
