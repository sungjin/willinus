/*
 * Initialize media source for recording
 */
var mediaSource = new MediaSource();
mediaSource.addEventListener('sourceopen', handleSourceOpen, false);
var mediaRecorder, recordedBlobs, sourceBuffer;
var userVideo = document.querySelector('video#usercapture');
var constraints = {
  audio: false,
  video: true
};

/*
 * Initialize variable and constant for processing
 */
 // We can change pixel intensity difference threshold
const thr= 1;
// temporary value for face center
var faceCenter= [160, 320];
var count= 0, isEmpty= true;
var results= new Array(8);
var result= new Array(3);
for(var i=0; i<3; i++) result[i]= new Array();
function getResult(){ return result; }
function getResults(){ return results; }

(function(){
  diff= new Array(320);
  prevImg= new Array(320);
  for(var i=0; i<320; i++){
    diff[i]= new Array(640);
    prevImg[i]= new Array(640);
    for(var j= 0; j<640; j++) diff[i][j]= 0;
  }
  histo= new Array();
})();

/*
 * Event Method : onSuccess
 */
function handleSuccess(stream) {
  console.log('getUserMedia() got stream: ', stream);
  window.stream = stream;
  if (window.URL) {
    userVideo.src = window.URL.createObjectURL(stream);
  } else {
    userVideo.src = stream;
  }
}

/*
 * Event Method : onError
 */
function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}

/*
 * Set up the Media Device with option and events
 */
navigator.mediaDevices.getUserMedia(constraints).
    then(handleSuccess).catch(handleError);

/*
 * Event Method : onSourceOpen
 */
function handleSourceOpen(event) {
  console.log('MediaSource opened');
  sourceBuffer = mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
  console.log('Source buffer: ', sourceBuffer);
}

/*
 * Event Method : onDataAvailable
 */
function handleDataAvailable(event) {
   // If capture the frame from Webcam
  if (event.data && event.data.size > 0) {
    count++;
    // then push the stream to temporary array for saving webm file
    recordedBlobs.push(event.data);
    // Generate the grayscale image with captured frame
    rgb2grayimg(event.data);
    // If count modular with 3, then this means we got data for 30 frames
    // Adjust modular value for changing # of capturing frame
    if(count%30 == 0) sendProcessing();
  }
}

/*
 * Event Method : onStop
 */
function handleStop(event) {
  console.log('Recorder stopped: ', event);
}

/*
 * Recording start with Movie start
 */
function startRecording() {
  // Initialize array for storing the captured frames
  recordedBlobs = [];
  // Set up the options for video recording
  var options = {mimeType: 'video/webm;codecs=vp9'};
  if (!MediaRecorder.isTypeSupported(options.mimeType)) {
    console.log(options.mimeType + ' is not Supported');
    options = {mimeType: 'video/webm;codecs=vp8'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.log(options.mimeType + ' is not Supported');
      options = {mimeType: 'video/webm'};
      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.log(options.mimeType + ' is not Supported');
        options = {mimeType: ''};
      }
    }
  }
  try {
    // Create MediaRecorder Object with options defined above
    mediaRecorder = new MediaRecorder(window.stream, options);
  } catch (e) {
    console.error('Exception while creating MediaRecorder: ' + e);
    alert('Exception while creating MediaRecorder: '
      + e + '. mimeType: ' + options.mimeType);
    return;
  }
  console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
  // Set up the media recorder with events defined above
  mediaRecorder.onstop = handleStop;
  mediaRecorder.ondataavailable = handleDataAvailable;
  // Capture the frame for 10ms
  // Adjust the value in side of start for changing capturing interval
  mediaRecorder.start(10);
  console.log('MediaRecorder started', mediaRecorder);

  isEmpty= true;
}

/*
 * Recording stop with moving the page
 */
function stopRecording(index) {
  // If we got a data, then push it into the results array
  if(result) {
    results[index]= clone(result);
    result= new Array(3);
    for(var i=0; i<3; i++) result[i]= new Array();
  }
  mediaRecorder.stop();
  isEmpty= true;
}

/*
 * Clone the object
 */
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

/*
 * Generate a file from the recorded data
 */
function video_download(index) {
  blobs[parseInt(index)]= clone(recordedBlobs);
  var fs= require('fs');
  var path= __dirname+'/result/' + index + '.webm';
  fs.writeFile(path, recordedBlobs, 'binary', function(err){
    if (err) throw err
    console.log('File saved.')
  });
}

/*
 * Send the data for taking a result of algorithm
 */
function sendProcessing(){
  // Generate the histogram
  for(var i=0; i<320; i++){
    for(var j=0; j<640; j++){
      if(diff[i][j] < 30 && diff[i][j] >= 0) histo.push((diff[i][j]).toFixed(1));
    }
  }

  var client= new Net.Socket();
  client.connect(20002, '223.194.69.103', function(){
    // Send the histogram data formatted string from array
    client.write(histo.slice(0, 15).toString().replace(/,/gi, " "), function(){
      console.log("data send");
    });
  });
  // If we got the result of algorithm
  client.on('data', function(data){
  // then push the result of the algorithm. Function defined below...
    setResult(data);
  });
  // If we got error for sending the data...
  client.on('error', function(e){
    console.log("exception : "+e);
  });
  // Task after socket closed
  client.on('close', function(e){
    console.log("closed");
  });
}

/*
 * Push the taken result into result array
 */
function setResult(data){
  // Convert data to String and split white-space
  var d= data.toString().split(' ');
  getResult()[1].push(parseFloat(d[0]));
  getResult()[0].push(parseFloat(d[1]));
  getResult()[2].push(parseFloat(d[2]));

  // Initialize the histogram
  histo= new Array();
  for(var i=0; i<320; i++)
    for(var j=0; j<640; j++) diff[i][j]= 0;
}

/*
 * If binding the face detection, get Histogram from this function
 *   and remove line 182~186 in this file
 */
function getHistogramValue(){
  var startx, endx, starty, endy;
  if(faceCenter[0] < 100){
    startx= 0; endx= 100+faceCenter[0];
  } else if(cfaceCnter[0] > 540){
    startx= faceCenter[0]-100; endx= 640;
  } else { startx= faceCenter[0]-100; endx= faceCenter[0]-100; }
  if(faceCenter[1] < 100){
    starty= 0; endy= 100+faceCenter[1];
  } else if(faceCenter[1] > 220){
    starty= faceCenter[1]-100; endy= 640;
  } else { starty= faceCenter[1]-100; endy= faceCenter[1]-100; }

  for(var i=starty; i<endy; i++){
    for(var j=startx; j<endx; j++){
      if(diff[i][j] < 15 && diff[i][j] > 1) histo.push(diff[i][j]);
    }
  }
}

/*
 * Calculate the difference of previous, current frames
 */
function calculateFrameDiff(current){
  for(var y= 0; y<current.height; y++){
    for(var x= 0; x<current.width; x++)
      // Using above threshold value defined constant
      if(Math.abs(prevImg.data[y][x] - current.data[y][x]) > thr) diff[y][x]++;
  }
}

/*
 * Generate grayscale image from rgb image by capturing frame
 */
function rgb2grayimg(stream) {
    var canvas = document.createElement('canvas');
    var canvasContext = canvas.getContext('2d');
    var img= new Image();
    img.src= URL.createObjectURL(stream);

    var imgW = stream.width;
    var imgH = stream.height;
    // for resize the captured frame
    canvas.width = 640;
    canvas.height = 320;

    canvasContext.drawImage(img, 0, 0, imgW, imgH, 0, 0, canvas.width, canvas.height);
    var imgPixels = canvasContext.getImageData(0, 0, canvas.width, canvas.height);

    // Calculate for generate grayscale
    for(var y = 0; y < imgPixels.height; y++){
        for(var x = 0; x < imgPixels.width; x++){
            var i = (y * 4) * imgPixels.width + x * 4;
            var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
            imgPixels.data[i] = avg;
            imgPixels.data[i + 1] = avg;
            imgPixels.data[i + 2] = avg;
        }
    }

    // If previous frame does not exist (first frame)
    if(isEmpty){
      prevImg= new Array(320);
      for(var i=0; i<320; i++) prevImg[i]= new Array(640);
      isEmpty= false;
    } else
      // If previous frame exists, then calculate the frequency
      calculateFrameDiff(imgPixels);
    // After calculating the frequency, replace previous with current
    prevImg= canvasContext.createImageData(640, 320);
    prevImg.data.set(imgPixels.data);
}
