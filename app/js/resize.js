'use strict';

(function(){
  let resize = function() {
    let sliderContainer = document.getElementById('slider-container')
    let ratio
    let consider = innerWidth / innerHeight
    if(consider <= 16/9) { // w >= h
      ratio = [innerWidth / 1920, innerWidth / 16 * 9 / 1080]
    } else { // w < h
      ratio = [innerHeight / 9 * 16 / 1920, innerHeight / 1080]
    }

    sliderContainer.setAttribute('style', `transform: scale(${ratio.join(',')})`)
  }

  window.addEventListener('resize', resize)
  window.addEventListener('load', resize)
})()
