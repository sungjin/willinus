csi-demo
========

Run w/o packaging
-----------------

1. install `node`
2. `git clone` this repo
3. `npm install .`
4. `npm install -g electron-prebuilt`
5. `electron .`

How to Exit or open devTools
----------------------------

(configurable via `config.konami`: `exit` or `devtools`, or null.)

use [Konami Command](https://ko.wikipedia.org/wiki/코나미_커맨드).

* B = left of ↑
* A = right of ↑
